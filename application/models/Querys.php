<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        $this->ajustes = $this->db->get('ajustes')->row();
    }
    
    function fecha($val)
    {
        return date($this->ajustes->formato_fecha,strtotime($val));
    }
    
    function moneda($val)
    {
        return $val.' '.$ajustes->moneda;
    }
    
    //Valida si tiene acceso a algun controaldor
    function has_access($controller,$function = '')
    {
        if(!is_numeric($controller))
            $con = $this->db->get_where('controladores',array('nombre'=>$controller));
        
        if(!empty($function) && !is_numeric($function) && (!empty($con) && $con->num_rows()>0 || is_numeric($controller)))
            $fun = $this->db->get_where('funciones',array('nombre'=>$function,'controlador'=>$con->row()->id));
        
        $controller = !empty($con) && $con->num_rows()>0?$con->row()->id:$controller;
        $function = !empty($fun) && $fun->num_rows()>0?$fun->row()->id:$function;        
        
        if($_SESSION['cuenta']!=1)
        {            
            if(!empty($function))
            return $this->db->get_where('roles',array('controlador'=>$controller,'funcion'=>$function,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
            else
            return $this->db->get_where('roles',array('controlador'=>$controller,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
        }
        else
            return TRUE;
    }
    
    function get_menu($controlador,$returnforeach = FALSE)
    {
        $str = '';
        $this->db->select('controladores.id as controlador,funciones.etiqueta as label, funciones.id as funcion_id, controladores.nombre, funciones.nombre as funcion');
        $this->db->join('funciones','funciones.controlador = controladores.id');
        $menu = $this->db->get_where('controladores',array('controladores.nombre'=>$controlador,'funciones.visible'=>1));
        if($menu->num_rows()>0 && $this->has_access($menu->row()->controlador)){
        $str .= '';
        foreach($menu->result() as $m){
        if($this->has_access($m->controlador,$m->funcion_id)){
        $label = empty($m->label)?ucwords($m->funcion):$m->label;
        $str.= '<li><a href="'.base_url($controlador.'/'.$m->funcion).'">'.$label.'</a></li>';
        }
        }
        $str.= '';
        }
        if(!$returnforeach)
        return $str;
        else return $menu->result();
    }
    //Funcion para saber cuantos productos hay disponibles para distribuir entre las sucursales de la compra realizada
    function get_disponible($compra){
        
            $this->db->select('SUM(distribucion.cantidad) as p');
            $productosucursal = $this->db->get_where('distribucion',array('compra'=>$compra));
            
            $this->db->select('SUM(compradetalles.cantidad) as p');
            $compradetalles = $this->db->get_where('compradetalles',array('compra'=>$compra));
            
            if($compradetalles->num_rows()>0 && $productosucursal->num_rows()>0)
            return $compradetalles->row()->p-$productosucursal->row()->p;
            
    }
    
    //Traer el nuevo nro de factura para realizar una venta en la caja seleccionada.
    function get_nro_factura(){
        $sucursal = $_SESSION['sucursal'];
        if(empty($_SESSION['caja'])){
            $caja = $this->db->get_where('cajas',array('sucursal'=>$sucursal));            
        }
        else
            $caja = $this->db->get_where('cajas',array('id'=>$_SESSION['caja']));
        if($caja->num_rows()>0){
            $caja = $caja->row();
            if(empty($_SESSION['caja']))$_SESSION['caja'] = $caja->id;
            $serie = $caja->serie;
            
            $ultima_venta = $this->db->get_where('cajadiaria',array('caja'=>$caja->id,'abierto'=>1));
            $ultima_venta = $serie.($ultima_venta->row()->correlativo+1);
            return $ultima_venta;
        }
        else
            header("Location:".base_url('panel/selsucursal').'?redirect='.$this->router->fetch_class().'/ventas');
    }

    function validar_stock(){
        $reporte = $this->db->get_where('newreportes',array('id'=>63));
        if($reporte->num_rows()>0){
            /*$reporte = $reporte->row();
            $reporte->query = $this->cleanData($reporte->query);
            $reporte->query = str_replace('consulta=','',$reporte->query);
            $reporte->query = trim($reporte->query);
            $reporte->query = str_replace('|selec|','SELECT',$reporte->query); //Quitamos la validación XSS
            $this->db->query($reporte->query);*/
            if(empty($this->user->sucursal)){
            	return $this->db->get_where('user',array('id'=>'-1'));
            }
            $consult = "
                SELECT    
                verificacion.idstock,             
                verificacion.cod_producto, 
                verificacion.producto, 
                verificacion.stock_inventario, 
                verificacion.stock_consulta, 
                verificacion.verificacion 
                FROM(
                SELECT 
                productosucursal.id as idstock,
                productosucursal.producto as cod_producto, 
                productos.nombre_comercial as producto, 
                stock_consulta.stock_consulta as stock_consulta, 
                productosucursal.stock as stock_inventario, 
                if(stock_consulta.stock_consulta=productosucursal.stock,1,0) as verificacion 
                FROM(
                SELECT 
                stock.cod_producto, 
                sum(stock.compra) as compra, 
                sum(stock.Ventas) as venta, 
                sum(stock.Ajuste_Entrada) as ajuste_entrada, 
                sum(stock.Ajuste_Salida) as ajuste_salida, 
                (sum(stock.compra) + sum(stock.Ajuste_Entrada)) - (sum(stock.Ventas)+sum(stock.Ajuste_Salida)) as stock_consulta 
                FROM(
                SELECT
                movimiento.cod_producto as cod_producto,
                if(movimiento.Movimiento = 'compra',Cantidad,0) as Compra,
                if(movimiento.Movimiento = 'venta',Cantidad,0) as Ventas,
                if(movimiento.Movimiento = 'ajuste_entrada',Cantidad,0) as Ajuste_Entrada,
                if(movimiento.Movimiento = 'ajuste_salida',Cantidad,0) as Ajuste_Salida
                FROM(
                SELECT
                'venta' as Movimiento, 
                ventas.id as Nro_registro, 
                ventas.fecha as Fecha, 
                productos.codigo as cod_producto, 
                ventadetalle.cantidad as Cantidad
                FROM ventas
                INNER JOIN ventadetalle on ventadetalle.venta = ventas.id
                INNER JOIN productos on productos.codigo = ventadetalle.producto
                WHERE (ventas.status = 0 or ventas.status is null) AND ventas.sucursal = ".$this->user->sucursal."
                UNION ALL 
                SELECT
                'compra' as Movimiento, 
                compras.id as Nro_registro, 
                compras.fecha as Fecha, 
                productos.codigo as cod_producto, 
                compradetalles.cantidad as Cantidad
                FROM compras
                INNER JOIN compradetalles on compradetalles.compra = compras.id
                INNER JOIN productos on productos.codigo = compradetalles.producto
                WHERE (compras.status = 0 or compras.status is null) AND compras.sucursal = ".$this->user->sucursal."
                UNION ALL 
                SELECT
                'ajuste_entrada' as Movimiento, 
                entrada_productos.id as Nro_registro, 
                entrada_productos.fecha as Fecha,
                productos.codigo as cod_producto, 
                entrada_productos_detalles.cantidad as Cantidad
                FROM entrada_productos
                INNER JOIN entrada_productos_detalles on entrada_productos_detalles.entrada_producto = entrada_productos.id
                INNER JOIN productos on productos.codigo = entrada_productos_detalles.producto
                WHERE entrada_productos_detalles.sucursal = ".$this->user->sucursal."
                UNION ALL 
                SELECT
                'ajuste_salida' as Movimiento, 
                salidas.id as Nro_registro, 
                salidas.fecha as Fecha, 
                productos.codigo as cod_producto, 
                salida_detalle.cantidad as Cantidad
                FROM salidas
                INNER JOIN salida_detalle on salida_detalle.salida = salidas.id
                INNER JOIN productos on productos.codigo = salida_detalle.producto
                WHERE salida_detalle.sucursal = ".$this->user->sucursal."
                ) as movimiento                
                )as stock                 
                GROUP by stock.cod_producto
                ) AS stock_consulta 
                INNER JOIN productosucursal on productosucursal.producto = stock_consulta.cod_producto 
                INNER JOIN productos ON productos.codigo = productosucursal.producto 
                WHERE productos.categoria_id != 1) as verificacion 
                where verificacion.verificacion = 0 
            ";
            $query = $this->db->query($consult);
            if($query->num_rows()>0){
                //$this->ajustar_stock($query);
                return $this->db->query($consult);
            }else{         
                return $query; 
            }
        }
    }

    function ajustar_stock($query){
        foreach($query->result() as $q){
            $this->db->update('productosucursal',array('stock'=>$q->stock_consulta),array('id'=>$q->idstock));
        }
    }
}
?>
