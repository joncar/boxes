<?php

require_once APPPATH.'/controllers/Panel.php';    

class Boxes extends Panel {

    function __construct() {
        parent::__construct();             
    }
        
        public function marcas($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);            
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }

    public function modelos($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);            
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }

    public function color_vehiculo($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);            
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }

    public function tipo_combustible($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);            
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
