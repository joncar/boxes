<?php

require_once APPPATH.'/controllers/Panel.php';    

class Ventas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }                
    }
    
    public function ventas($x = '', $y = '') {
        $this->load->library('enletras');        
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2');
        $crud->unset_read()
                ->unset_delete();
        $crud->columns('transaccion', 'cliente', 'fecha', 'caja', 'nro_factura', 'total_venta', 'status');
        $crud->callback_column('nro_factura', function($val, $row) {
            return '<a href="javascript:showDetail('.$row->id.')">'.$val.'</a>';
        });
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '0':return $this->user->admin==1?'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>':'Activa';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        if($this->user->admin!=1){
            $crud->unset_edit();
        }
        $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
                ->set_relation('cliente', 'clientes', '{nombres} {apellidos}')
                ->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->where('ventas.sucursal', $_SESSION['sucursal']);
        if (!empty($_SESSION['caja'])){
            $crud->where('ventas.caja', $_SESSION['caja']);
        }
        $crud->add_action('Creditos','',base_url('movimientos/creditos/creditos').'/');
        $crud->order_by('id','DESC');

        $output = $crud->render();
        $output->crud = 'ventas';
        if ($x == 'add') {
            $detalles = array();
            if(is_numeric($y)){
                $servicios = $this->db->get_where('servicios',array('id'=>$y));
                if($servicios->num_rows()>0){                   
                        $servicios = $servicios->row();                
                        $this->db->select('servicios_detalles.*,productos.codigo,productos.nombre_comercial, productos.iva_id as iva, productos.inventariable');                    
                        $this->db->join('productos','productos.id = servicios_detalles.productos_id');
                        $servicios->detalles = $this->db->get_where('servicios_detalles',array('servicios_id'=>$y));
                        $servicios->total_venta = 0;
                        foreach($servicios->detalles->result() as $x){
                            $servicios->total_venta+= $x->total;
                        }                    
                        $detalles = $servicios;                    
                }
            }
            $output->output = $this->load->view('ventas', array('servicio'=>$detalles), TRUE);
        }
        if ($x == 'edit' && !empty($y) && is_numeric($y)) {
            $venta = $this->db->get_where('ventas', array('id' => $y));
            $this->db->select('ventadetalle.*, productos.inventariable')
                     ->join('productos','productos.codigo = ventadetalle.producto');
            $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
            $output->output = $this->load->view('ventas', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
        }
        if ($x == 'imprimir' || $x=='imprimir2' || $x == 'imprimirticket') {
            if (!empty($y) && is_numeric($y)) {
                $this->db->select('ventas.*, sucursales.denominacion, sucursales.telefono, sucursales.direccion, cajas.denominacion as caja, clientes.nombres as clientename, clientes.apellidos as clienteadress');
                $this->db->join('clientes', 'clientes.id = ventas.cliente');
                $this->db->join('cajas', 'cajas.id = ventas.caja');
                $this->db->join('sucursales', 'sucursales.id = ventas.sucursal');
                $venta = $this->db->get_where('ventas', array('ventas.id' => $y));
                if ($venta->num_rows() > 0) {
                    $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
                    if ($x == 'imprimir' || $x=="imprimir2" || $x=="imprimirticket") {
                        $view = $x=="imprimir"?"imprimir_factura":"imprimir_factura_linea";
                        $margen = $x=="imprimir"?array(5, 1, 5, 8):array(2, 5, 5, 8);
                        $papel = 'L';
                        if($x=="imprimir" || $x=="imprimir2"){
                            if(!isset($_GET['pdf']) || $_GET['pdf']!=0){
                                ob_clean();
                                $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', $margen);
                                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                                $html2pdf->Output('Factura Legal.pdf', 'D');
                            }else{
                                $this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles));
                            }
                        } else {
                            $output = $this->load->view('reportes/imprimirTicket', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
                            echo $output;
                        }
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            }
        } else {
            $output->output = $this->load->view('ventas_list',array('output'=>$output->output),TRUE);
            $this->loadView($output);
        }
    }

    function ventas_detail($ventaid){
        $this->as['ventas_detail'] = 'ventadetalle';
        $crud = $this->crud_function('','');
        $crud->where('venta',$ventaid);
        $crud->columns('producto','cantidad','totalcondesc');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->roW()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $crud = $crud->render();
        echo $crud->output;

    }
    
    function next_nro_factura(){
        echo $this->querys->get_nro_factura();
    }
    /* Cruds */
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
