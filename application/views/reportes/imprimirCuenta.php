<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>Factura</title>
                
        </head>
        <body style='font-size:11px; width:227px; margin:37px;'>
            <h3 align="center" style="font-size:20px; font-weight:bold; margin-bottom:5px"><?= $venta->denominacion ?></h3>            
            <table style='width:100%; font-size:11px;'>
                <tr><td><b>Nro. Venta: </b><?= $venta->id ?></td><td><b>Cajero/a</b> <?= $_SESSION['nombre'] ?></td></tr>
            </table>
            <div><b>Fecha: </b><?= date("d/m/Y H:i:s",strtotime($venta->fecha)) ?></div>        
            <!---- Si es delivery --->
            <?php if($venta->tipo_pedidos_id==2): ?>                
            <div style="text-align:left">Entregar en <b><?= $venta->direccion_entrega_pedido ?></b></div>
                <?php $cliente = $this->db->get_where('clientes',array('id'=>$venta->clientes_id))->row(); ?>
                <div style="text-align:left">Cliente  <b><?= empty($cliente)?'No almacenado':$cliente->nombres.' '.$cliente->apellidos ?></b></div>
                <div style="text-align:left">Télefono  <b><?= empty($cliente)?'No almacenado':$cliente->celular1 ?></b></div>
            <?php endif ?> 
            <!--- Si es mesa --->
            <?php if($venta->tipo_pedidos_id==1): ?>                
            <div style="text-align:left">Mesa # <b><?= $this->db->get_where('mesas',array('id'=>$venta->mesas_id))->row()->mesa_nombre ?></b></div>
            <?php endif ?>
        <div>
            <table cellspacing="10" style="font-size:11px;">
                <thead>
                    <tr style="border-top:1px solid black; border-bottom:1px solid black">
                        <th style="width:40%;">Descripcion</th>
                        <th style="width:20%; text-align:center;">Cant.</th>
                        <th style="width:20%; text-align:center;">Precio Unit.</th>
                        <th style="width:20%; text-align:center;">Total</th>
                    </tr>
                </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        <?php foreach($detalles->result() as $d): ?>
                            <tr><td><?= $this->db->get_where('productos',array('codigo'=>$d->producto))->row()->nombre_comercial ?></td><td align="right"><?= $d->cantidad ?></td><td align="right"><?= $d->precioventa ?></td><td align="right"><?= $d->totalcondesc ?></td></tr>
                            <?php $total+= $d->total; ?>
                        <?php endforeach ?>
                            <tr><td style="border-top:1px solid black">Total Venta: </td><td style="border-top:1px solid black" colspan='3' align="right"><?= number_format($total,0,',','.').' Gs' ?></td></tr>
                            <!---- Si es delivery --->
                            <?php if($venta->tipo_pedidos_id==2): ?>  
                                <tr><td>Efectivo: </td><td colspan='3' align="right"><?= number_format($venta->efectivo,0,',','.').' Gs' ?></td></tr>
                                <tr><td>Vuelto: </td><td colspan='3' align="right"><?= number_format($venta->vuelto,0,',','.').' Gs' ?></td></tr>
                            <?php endif ?>
                    </tbody>
                </table>
        </div>
        <p align='center' style="margin:10px; font-size:14px;"><i>Agradecemos su preferencia</i></p>
        </body>
        <script>
            window.print();
        </script>
</html>
