<html lang="es"><head>
        <title>Factura</title>
        <meta charset="utf-8">
      
</head>
<body style='font-size:11px; width:227px; margin:37px;'>
    <h3 align="center" style="font-size:20px; font-weight:bold; margin-bottom:5px"><?= $venta->denominacion ?></h3>
    <div align="center"><?= $venta->direccion ?></div>
    <div align="center">Telef. <?= $venta->telefono ?></div>
    <h3 align="center" style="border-bottom:1px solid black; border-top:1px solid black; font-size:20px; font-weight:bold; margin-bottom:5px;">Recibo de Pago</h3>
    <table style='width:100%; font-size:11px;'>
        <tr><td><b>Id. Pago: </b><?= $venta->id ?></td><td><b>Cajero/a</b> <?= $_SESSION['nombre'] ?></td></tr>
    </table>
<div><b>Fecha: </b><?= date("d/m/Y H:i:s",strtotime($venta->fecha)) ?></div>
<div><b>Caja: </b><?= $venta->caja ?></div>
<div><b>Cliente: </b><?= $venta->clientename.' '.$venta->clienteadress ?></div>
<div><b>Saldo anterior: </b><?= number_format($venta->saldo+$venta->totalpagado,2,',','.'); ?></div>
<div><b>Monto Pagado: </b><?= number_format($venta->totalpagado,2,',','.'); ?></div>
<div><b>Saldo actual: </b><?= number_format($venta->saldo,2,',','.'); ?></div>

<p align='center' style="margin:10px; font-size:14px;"><i>Gracias por su pago</i></p>
</body>
<script>
    window.print();
</script>
</html>
