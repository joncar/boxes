<?php if(empty($_POST)): ?>
<div class="container">
    <h1 align="center"> Resumen de ventas</h1>
<form action="<?= base_url('reportes/listado_inventario_categoria') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una categoria</label>
        <?= form_dropdown_from_query('categoria','categoriaproducto','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['categoria']))$cliente = $this->db->get_where('categoriaproducto',array('id'=>$_POST['categoria']))->row()->denominacion; ?>
<? if(!empty($_POST['sucursal']))$sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']))->row()->denominacion; ?>
    <h1 align="center"> Listado de inventario</h1>
    <p><strong>Sucursal: </strong> <?= empty($_POST['sucursal'])?'Todos':$sucursal ?></p>
    <p><strong>Categoria: </strong> <?= empty($_POST['categoria'])?'Todos':$cliente ?></p>    
    
    <table class="table" width="100%" style="font-size:12px;">
        <thead>
                <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Cantidad</th> 
                        <th>P.Venta</th>
                        <th>P.Venta|Con Desc.</th>
                </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($_POST['categoria']))$this->db->where('productos.categoria_id',$_POST['categoria']);
                if(!empty($_POST['sucursal']))$this->db->where('productosucursal.sucursal',$_POST['sucursal']);
                $total = 0;
                $total_debito = 0;
                $total_credito = 0;
                $this->db->select('productosucursal.producto as codigo, productos.nombre_comercial as producto, SUM( productosucursal.stock ) AS stockTotal, productos.precio_venta, productos.descmax',false);
                $this->db->join('productos','productos.codigo = productosucursal.producto');
                $this->db->group_by('productosucursal.producto');
                $this->db->having('stockTotal > ',0);
                $this->db->order_by('productos.nombre_comercial','ASC');
                $ventas = $this->db->get('productosucursal');
                
            ?>
            <?php 
                foreach($ventas->result() as $c): 
                $c->descuento = $c->precio_venta - (($c->precio_venta *$c->descmax)/100);
                ?>
                <tr>                        
                        <td><?= $c->codigo ?></td>
                        <td><?= $c->producto ?></td>
                        <td><?= $c->stockTotal ?></td>
                        <td><?= number_format($c->precio_venta,0,',','.') ?></td>
                        <td><?= number_format($c->descuento,0,',','.') ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
<?php endif; ?>