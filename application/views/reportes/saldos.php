<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Resumen de saldos</h1>
<form action="<?= base_url('reportes/saldos') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>  
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>    
        <?php
            $_POST['desde'] = !empty($_POST['desde'])?date("Y-m-d",strtotime(str_replace("/","-",$_POST['desde']))):'';
            $_POST['hasta'] = !empty($_POST['hasta'])?date("Y-m-d",strtotime(str_replace("/","-",$_POST['hasta']))):'';                
            if(!empty($_POST['desde']))$this->db->where('ventas.fecha >=',$_POST['desde']);
            if(!empty($_POST['hasta']))$this->db->where('ventas.fecha <=',$_POST['hasta']);

            $this->db->where('transaccion','2');
            $this->db->where('ventas.status != ',-1);
            $this->db->group_by('ventas.cliente');
            $this->db->select('clientes.id as cliente, clientes.nro_documento as cedula, clientes.nombres, clientes.apellidos, SUM(ventas.total_venta) as total_venta, SUM(pagocliente.totalpagado) as pagocliente');
            $this->db->join('pagocliente','pagocliente.cliente = ventas.cliente','left');
            $this->db->join('clientes','clientes.id = ventas.cliente');
            $ventas = $this->db->get('ventas');
        ?>
        <?php 
            $total = 0;
            foreach($ventas->result() as $n=>$c): 
                    $this->db->order_by('fecha','DESC');                             
                    $cliente = $this->db->get_where('pagocliente',array('cliente'=>$c->cliente)); 

                    get_instance()->db->select('SUM(pagocliente.totalpagado) as total_pagos');            
                    $p = get_instance()->db->get_where('pagocliente',array('pagocliente.cliente'=>$c->cliente));
                    $totalpagos =  $p->num_rows==0?(string)'0':(string)$p->row()->total_pagos;            

                    get_instance()->db->select('SUM(ventas.total_venta) as total_venta');            
                    get_instance()->db->where('transaccion','2');
                    $p = get_instance()->db->get_where('ventas',array('ventas.cliente'=>$c->cliente));
                    $totalventa =  $p->row()->total_venta==null?(string)0:$p->row()->total_venta;
                    $ventas->row($n)->totalventa = $totalventa;
                    $ventas->row($n)->totalpagos = $totalpagos;
                    $total+= $totalventa-$totalpagos;
            endforeach;
        ?>
    <h1 align="center"> Resumen de saldos</h1>    
    <p style="font-size:12px;"><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':$_POST['desde'] ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':$_POST['hasta'] ?> <b>Total Saldo: </b><?= number_format($total,0,',','.') ?></p>
    
    <table border="0" cellspacing="18" class="table" width="100%" style="font-size:12px;">
        <thead>
                <tr>
                    <th>Cédula</th>
                    <th>Cliente</th>
                    <th>Ultima Fech. Compra</th>
                    <th>Ultima Fech. Pago</th>
                    <th>Total Ventas</th>
                    <th>Total Pagos</th>
                    <th>Saldo</th>
                </tr>
        </thead>
        <tbody>            
            <?php foreach($ventas->result() as $n=>$c): ?>
                <tr>
                        <td><?= $c->cedula ?></td>
                        <td><?= $c->nombres.' '.$c->apellidos ?></td>
                        <td><? $this->db->order_by('fecha','DESC'); echo date("d/m/Y",strtotime($this->db->get_where('ventas',array('cliente'=>$c->cliente,'transaccion'=>2))->row()->fecha)) ?></td>
                        <td>
                            <? 
                            if($cliente->num_rows>0){                            
                            echo date("d/m/Y",strtotime($cliente->row()->fecha));
                            }
                            else echo 'N/A'?>
                        </td>
                        <td align="center"><?= empty($c->totalventa)?0:number_format($c->totalventa,0,',','.'); ?></td>                        
                        <td align="center"><?= empty($c->totalpagos)?0:number_format($c->totalpagos,0,',','.'); ?></td>                        
                        <td align="center"><?= number_format($c->totalventa-$c->totalpagos,0,',','.'); ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
<?php endif; ?>