<!Doctype html>
<html lang="es">
    <head>
            <title><?= empty($title)?'Boxes':$title.'' ?></title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">            
            <link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/logos/'.$favicon ?>" />    
            <link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/logos/'.$favicon ?>" />
            <?php if(empty($crud) || empty($css_files) || !empty($loadJquery)): ?>            
            <script src="http://code.jquery.com/jquery-2.1.0.js"></script>      
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                
            <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
            <?php endif ?>
            <?php 
            if(!empty($css_files) && !empty($js_files)):
            foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
            <?php endforeach; ?>
            <?php foreach($js_files as $file): ?>
            <script src="<?= $file ?>"></script>
            <?php endforeach; ?>                
            <?php endif; ?>
         <script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">    
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <?php if(empty($_SESSION['user'])): ?>
            <link href="<?= base_url('css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
            <link href="<?= base_url('css/components.min.css') ?>" rel="stylesheet" id="style_components" type="text/css" />
            <link href="<?= base_url('css/plugin.min.css') ?>" rel="stylesheet" type="text/css" />
            <link href="<?= base_url('css/login.css') ?>" rel="stylesheet" type="text/css" />
        <?php else: ?>
            <link rel="stylesheet" type="text/css" href="<?= base_url('css/ace.min.css') ?>">
            <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">
            <script src="<?= base_url('js/ace-extra.min.js') ?>"></script>  
            <script src="<?= base_url().'js/frame.js' ?>"></script>
            <?php $this->load->view('predesign/multiselect') ?>
        <?php endif ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58932801-4"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-58932801-4');
        </script>

    </head>  
    <?php $this->load->view($view) ?>                  
    <script>
        function updateCache(){
            $.get('<?= base_url('panel') ?>',{},function(data){
                setTimeout(updateCache,30000);
            });
        }
        $(document).on('ready',function(){
            updateCache();
        })
    </script>
</html>
