<?php
	$this->set_css($this->default_theme_path.'/bootstrap2/bootstrap/css/bootstrap.css');
    $this->set_css($this->default_theme_path.'/bootstrap2/css/flexigrid.css');
    $this->set_js($this->default_theme_path.'/bootstrap2/bootstrap/js/bootstrap.js');
	$this->set_js_lib($this->default_theme_path.'/bootstrap2/js/jquery.form.js');
	$this->set_js_config($this->default_theme_path.'/bootstrap2/js/flexigrid-edit.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');		
?>


<div class=" flexigrid crud-form" data-unique-hash="<?php echo $unique_hash; ?>">
	<?php echo form_open( $update_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
    	<a href="#home" aria-controls="home" role="tab" data-toggle="tab">Español</a>
    </li>
    <li role="presentation">
    	<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Catalan</a>
    </li>
    <li role="presentation">
    	<a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Ingles</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="home">
		<?php
            $counter = 0;
            foreach($fields as $field)
            {
                    $even_odd = $counter % 2 == 0 ? 'odd' : 'even';
                    $counter++;
                    $input = str_replace($field->field_name,$field->field_name.'_es',$input_fields[$field->field_name]->input);
                    if(!empty($field_values->es->{$field->field_name})){
                        $input = str_replace('value=""','value="'.$field_values->es->{$field->field_name}.'"',$input);
    					$input = str_replace('</textearea>',$field_values->es->{$field->field_name}.'</textearea>',$input);
                    }
                    $name = str_replace($field->field_name,$field->field_name.'_es',$field->field_name);
            ?>

            <div class='form-group' id="<?php echo $name; ?>_field_box">
                    <label for='field-<?= $name ?>' id="<?php echo $name; ?>_display_as_box"  style="width:100%">
                            <?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input ?>
            </div>
            <?php 
        	}
    	?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
    	<?php
            $counter = 0;
            foreach($fields as $field)
            {
                    $even_odd = $counter % 2 == 0 ? 'odd' : 'even';
                    $counter++;
                    $input = str_replace($field->field_name,$field->field_name.'_ca',$input_fields[$field->field_name]->input);
                    if(!empty($field_values->ca->{$field->field_name})){
                        $input = str_replace('value=""','value="'.$field_values->ca->{$field->field_name}.'"',$input);
    					$input = str_replace('</textearea>',$field_values->ca->{$field->field_name}.'</textearea>',$input);
                    }
                    $name = str_replace($field->field_name,$field->field_name.'_ca',$field->field_name);
            ?>

            <div class='form-group' id="<?php echo $name; ?>_field_box">
                    <label for='field-<?= $name ?>' id="<?php echo $name; ?>_display_as_box"  style="width:100%">
                            <?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input ?>
            </div>
            <?php 
        	}
    	?>

    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
    	<?php
            $counter = 0;
            foreach($fields as $field)
            {
                    $even_odd = $counter % 2 == 0 ? 'odd' : 'even';
                    $counter++;
                    $input = str_replace($field->field_name,$field->field_name.'_en',$input_fields[$field->field_name]->input);
                    if(!empty($field_values->en->{$field->field_name})){
                        $input = str_replace('value=""','value="'.$field_values->en->{$field->field_name}.'"',$input);
    					$input = str_replace('</textearea>',$field_values->en->{$field->field_name}.'</textearea>',$input);
                    }
                    $name = str_replace($field->field_name,$field->field_name.'_en',$field->field_name);
            ?>

            <div class='form-group' id="<?php echo $name; ?>_field_box">
                    <label for='field-<?= $name ?>' id="<?php echo $name; ?>_display_as_box"  style="width:100%">
                            <?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?> :
                    </label>
                    <?php echo $input ?>
            </div>
            <?php 
        	}
    	?>

    </div>
  </div>

		<div id='report-error' style="display:none" class='alert alert-danger'></div>
        <div id='report-success' style="display:none" class='alert alert-success'></div>
		<div class="btn-group">			
        <button id="form-button-save" type='submit' class="btn btn-success"><?php echo $this->l('form_save'); ?></button>
        <?php if(!$this->unset_back_to_list) { ?>
        <button type='button' id="save-and-go-back-button"  class="btn btn-default"><?php echo $this->l('form_save_and_go_back'); ?></button>                            
        <button type='button' id="cancel-button"  class="btn btn-danger"><?php echo $this->l('form_cancel'); ?></button>
    <?php } ?>
</div>
<?php echo form_close(); ?>
</div>
<script>

	var validation_url = '<?php echo $validation_url?>';

	var list_url = '<?php echo $list_url?>';



	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";

	var message_update_error = "<?php echo $this->l('update_error')?>";

        $("input[type='text'],input[type='password']").addClass('form-control');
    $(document).on('ready',function(){$('#home').tab('show')});
</script>